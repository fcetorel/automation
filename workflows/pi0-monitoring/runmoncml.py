#!/usr/bin/env python3
import sys
import os
import subprocess
# import ROOT if possible (use mock import for docs generation)
try:
    if importlib.util.find_spec('ROOT'):
        import ROOT
except:
    from unittest.mock import MagicMock, patch
    my_conddb = MagicMock()
    patch.dict("sys.modules", fake_root=my_conddb).start()
    import fake_root as ROOT
from typing import Optional, List
from ecalautoctrl import JobCtrl, HandlerBase, prev_task_data_source, process_by_fill
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase

@prev_task_data_source
@process_by_fill(fill_complete=True)
class Pi0MonCmlHandler(HandlerBase):
    """
    Update the cumulative pi0 mass history plot.

    :param task: task name.
    :param prev_input: name of the task from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """

    def __init__(self,
                 task: str,
                 prev_input: str,
                 deps_tasks: Optional[List[str]]=None,
                 **kwargs):
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)
        self.prev_input = prev_input

        self.submit_parser.add_argument('--eosplots',
                                        dest='eosplots',
                                        default=None,
                                        type=str,
                                        help='Plots webpage EOS path')
        self.submit_parser.add_argument('--plotsurl',
                                        dest='plotsurl',
                                        default=None,
                                        type=str,
                                        help='Plots webpage url')

        self.resubmit_parser.add_argument('--eosplots',
                                          dest='eosplots',
                                          default=None,
                                          type=str,
                                          help='Plots webpage EOS path')
        self.resubmit_parser.add_argument('--plotsurl',
                                          dest='plotsurl',
                                          default=None,
                                          type=str,
                                          help='Plots webpage url')
        
    def resubmit(self):
        """
        Mark failed run for reprocessing.
        """

        # get the new run to process
        runs = self.rctrl.getRuns(status = {self.task : 'processing'})
        # check if any job failed, if so mark all runs for reprocessing
        for run_dict in runs:
            jctrl = JobCtrl(task=self.task,
                            campaign=self.campaign,
                            tags={'run_number' : run_dict['run_number'],
                                  'fill' : run_dict['fill']},
                            dbname=self.opts.dbname)
            if jctrl.taskExist() and len(jctrl.getFailed())>0:
                self.rctrl.updateStatus(run=run_dict['run_number'], status={self.task : 'reprocess'})
                group = jctrl.getJob(jid=0, last=True)[-1]['group'] if 'group' in jctrl.getJob(jid=0, last=True)[-1] else ''
                if group:
                    for r in group.split(','):
                        self.rctrl.updateStatus(run=r, status={self.task : 'reprocess'}) 
                

    def submit(self):
        """
        Read the reco files and produce the monitoring plots.
        This submit step is meant to be run interactively.
        """

        macro = os.environ['CMSSW_BASE']+'/src/CalibCode/submit/monitoring/finalTimeVariationPlot.C'                    
        ROOT.gROOT.LoadMacro(macro)
        
        for group in self.groups():
            # master run
            run = group[-1]
            fdict = self.get_files(group)
            if fdict is not None and len(fdict)>0:
                jctrl = JobCtrl(task=self.task,
                                campaign=self.campaign,
                                tags={'run_number' : run['run_number'], 'fill' : run['fill']},
                                dbname=self.opts.dbname)
                if not jctrl.taskExist():
                    jctrl.createTask(jids=[0],
                                     fields=[{'group' : ','.join([r['run_number'] for r in group[:-1]])}])
                try:
                    jctrl.running(jid=0)
                    self.log.info(f'Processing fill {run["fill"]}.')
                    self.rctrl.updateStatus(run=run['run_number'], status={self.task : 'processing'})
                    for r in group[:-1]:
                        self.rctrl.updateStatus(run=r['run_number'], status={self.task : 'merged'})
                    eosdir = os.path.abspath(self.opts.eosplots)
                    plotsurl = os.path.abspath(self.opts.plotsurl)
                    os.makedirs(eosdir, exist_ok=True)

                    # set the path to the cumulative file
                    cum_mass_info = os.path.abspath(eosdir+'/pi0_fitMassInfo_cumulative.txt')
                    with open(cum_mass_info, 'a') as cfile:
                        for newf in fdict:
                            with open(newf, 'r') as rfile:
                                for i, l in enumerate(rfile):
                                    if i>0:
                                        cfile.write(l)                                
                                cfile.write('\n')
                                        
                    with open("timeVariationFileList.txt", "w") as finalin:
                        finalin.write('#TextFile,Label,Color\n')
                        finalin.write(f'{cum_mass_info},With Light Monitoring Correction,417')
                    ret = subprocess.run("conddb list EcalPulseShapes_prompt 2> /dev/null | grep 'EcalCond' | awk '{print $1\" \"$2\" \"$3}'", capture_output=True, shell=True)
                    ret = ret.stdout.decode('utf-8').strip()
                    psiovs = [l.split()[0] for l in ret.split('\n')]
                    psiov_times = [' '.join(l.split()[1:]) for l in ret.split('\n')]
                    ROOT.finalTimeVariationPlot("timeVariationFileList.txt", eosdir+'/', False, psiovs, psiov_times)

                    # mark as completed
                    jctrl.done(jid=0, fields={
                        'output' : cum_mass_info,
                        'plots' : plotsurl})

                except Exception as e:
                    jctrl.failed(jid=0)
                    self.log.error(f'Failed producing the monitoring plots for fill {run["fill"]}: {e}')
                    continue
    
if __name__ == '__main__':
    handler = Pi0MonCmlHandler(task='pi0-mon-cml',
                               deps_tasks=['pi0-mon'],
                               prev_input='pi0-mon')

    ret = handler()

    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(Pi0MonCmlHandler)
