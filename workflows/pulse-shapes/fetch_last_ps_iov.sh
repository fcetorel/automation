#!/bin/bash

set -x

lastiov=`conddb list EcalPulseShapes_prompt --limit 1 2>&- | grep PulseShape | awk '{print $1}'`
conddb_dumper -O EcalPulseShapes -t EcalPulseShapes_prompt -b $lastiov -n 1 -o dump.dat
cat dump_1.dat | sed -e "s/^/0 /1" | awk '{ s = ""; for (i = 1; i <= 4; i++) s = s $i " "; p = ""; for (j = 5; j < NF; j++) p = p $j " ";  print s$17" 0 "p }' > ref.txt

rm *dat
