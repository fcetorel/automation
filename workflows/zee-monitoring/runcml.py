#!/usr/bin/env python3
import sys
import os 
import itertools
import matplotlib.pyplot as plt
from typing import Optional, List
from ecalautoctrl import JobCtrl, prev_task_data_source, process_by_fill
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase

from zeemon_tools import ZeeMonPlots
from runjobs import ZeeMonHandler


class ZeeMonCmlHandler(ZeeMonHandler):
    """
    Execute all the steps to generate the Zee comulative monitoring plots.

    :param task: task name.
    :param prev_input: name of the task from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """

    def __init__(self,
                 task: str,
                 prev_input: str,
                 deps_tasks: Optional[List[str]] = None,
                 **kwargs):
        super().__init__(task=task,
                         prev_input=prev_input,
                         deps_tasks=deps_tasks,
                         **kwargs)

    def process_cml(self, files: List[str] = None):
        """
        Load preprocessed histograms and produce the comulative plots.

        :param files: list of histogram files.
        """
        # Initialize the plotting tool
        # extract only the main ntuples files (it's always the last one)
        plots = ZeeMonPlots(files=files,
                            categories={
                                'EB': {'color': 'darkslateblue'},
                                'EE': {'color': 'steelblue'}
                            },
                            load_data=files)

        path = os.path.abspath(f'{self.opts.eosplots}')
        os.makedirs(path, exist_ok=True)

        # get the plot
        fig = plots.scale_monitoring()
        fig.savefig(f'{path}/mee_scale_prompt_2023.png')
        plt.close(fig)

        # save processed data
        plots.save_histograms(f'{path}/histograms_prompt_2023.json')

        return f'{path}/histograms_prompt_2023.json', self.opts.plotsurl

    def resubmit(self) -> int:
        """
        Resubmit failed runs.

        :return: status.
        """
        # get the new run to process
        runs = self.rctrl.getRuns(status={self.task: 'processing'})
        # check if any job failed, if so resubmit
        for run in runs:
            jctrl = JobCtrl(task=self.task,
                            campaign=self.campaign,
                            tags={'run_number': run['run_number'],
                                  'fill': run['fill']},
                            dbname=self.opts.dbname)
            if jctrl.taskExist() and len(jctrl.getFailed()) > 0:
                files = jctrl.getJob(jid=0, last=True)[-1]['inputs']
                try:
                    jctrl.running(jid=0)
                    self.log.info(f'Resubmitting processing for run: {run["run_number"]}')
                    ret = self.process_cml(files=files.split(','))
                    if not ret:
                        jctrl.failed(jid=0)
                    else:
                        # mark as completed
                        jctrl.done(jid=0, fields={
                            'output': ret[0],
                            'plots': ret[1]})

                except Exception as e:
                    jctrl.failed(jid=0)
                    self.log.error(f'Failed producing the monitoring plots for fill {run["fill"]}: {e}')
                    continue

    def submit(self) -> int:
        """
        Submit new runs.

        :return: status.
        """
        # the comulative plot is produced using always all available data
        # hence we can merge multiple fills together
        group = list(itertools.chain.from_iterable(self.groups()))
        all_runs = self.rctrl.getRuns(status={self.prev_input: 'done'}, active=False)
        all_runs += self.rctrl.getRuns(status={self.prev_input: 'done'}, active=True)
        fdict = self.get_files(all_runs)

        if group and fdict is not None and len(fdict) > 0:
            # master run
            run = group[-1]
            jctrl = JobCtrl(task=self.task,
                            campaign=self.campaign,
                            tags={'run_number': run['run_number'], 'fill': run['fill']},
                            dbname=self.opts.dbname)
            if not jctrl.taskExist():
                jctrl.createTask(jids=[0],
                                 fields=[{
                                     'group': ','.join([r['run_number'] for r in group[:-1]]),
                                     'inputs': ','.join(fdict)}])
            try:
                jctrl.running(jid=0)
                self.rctrl.updateStatus(run=run['run_number'],
                                        status={self.task: 'processing'})
                for r in group[:-1]:
                    self.rctrl.updateStatus(run=r['run_number'],
                                            status={self.task: 'merged'})
                self.log.info('Processing comulative plots')
                ret = self.process_cml(files=fdict)
                if not ret:
                    jctrl.failed(jid=0)
                else:
                    # mark as completed
                    jctrl.done(jid=0, fields={
                        'output': ret[0],
                        'plots': ret[1]})

            except Exception as e:
                jctrl.failed(jid=0)
                self.log.error(f'Failed producing the comulative monitoring plots for fill {run["fill"]}: {e}')


if __name__ == '__main__':
    handler = ZeeMonCmlHandler(task='zee-mon-cml',
                               deps_tasks=['zee-mon'],
                               prev_input='zee-mon')

    ret = handler()

    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(ZeeMonCmlHandler)
    
