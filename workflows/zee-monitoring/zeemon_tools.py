"""Plotting Tools: plot invariant mass distribution from main ECALElf ntuples"""

from typing import List, Dict, Optional, Union
import json
import pandas as pd
import numpy as np
import awkward as ak
import uproot
import mplhep as hep
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import logging
plt.style.use(hep.style.CMS)

def truncated_mean(arr):
    """
    Compute the mean of the invariant mass distribution restricting to 2 sigma
    """
    
    # compute mean and standard deviation for each time bin
    bin_size = ak.num(arr.invMass_ECAL_ele)[0]
    stds = np.repeat(np.std(arr.invMass_ECAL_ele.to_numpy(), axis=1, keepdims=True),
                     bin_size, axis=1)
    mean = np.repeat(ak.mean(arr.invMass_ECAL_ele, axis=1, keepdims=True),
                     bin_size, axis=1)
    
    # truncate data within 2 std from the mean
    tr = np.abs(arr.invMass_ECAL_ele-mean) < 2*stds
        
    # note that due to a bug in the awkward->numpy overloading the std is still computed on the full set
    return ak.mean(arr.invMass_ECAL_ele, weight=tr, axis=1), ak.Array(np.std(arr.invMass_ECAL_ele.to_numpy(), axis=1)/np.sqrt(ak.num(arr.invMass_ECAL_ele)))

class NumpyArrayEncoder(json.JSONEncoder):
    """Helper class to encode NumPy arrays into json"""
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)

class ZeeMonPlots:
    """
    Produce scale stability and invariant mass distribution plots.

    :param files: input files.
    :param categories: dictionary defining the categories:
    { "name" : {"expr": function, "color": plot color}}
    :param lumi_file: luminosity data from brilcalc.
    :param load_data: if not false read data from pre-processed file.
    :param minutes: lenght in minutes of the luminosity bins.
    :param events_per_bin: number of events per bin.
    """

    def __init__(self,
                 files: List[str] = None,
                 categories: Dict[str, Dict] = None,
                 lumi_file: str = None,
                 load_data: Optional[Union[bool, Union[str, List[str]]]] = False,
                 minutes: Optional[float] = 600.,
                 events_per_bin: Optional[int] = 5000):
        self.data = {}
        self.colors = {k: v['color'] for k, v in categories.items()}

        # read back data from json files
        if load_data:
            self.load_histograms(load_data)
        else:
            if not (files and categories and lumi_file):
                logging.warning('No data provided')
            # load the mee data
            evts = uproot.lazy({f: 'selected' for f in files},
                               filter_name=['invMass_ECAL_ele',
                                            'etaSCEle',
                                            'eventTime',
                                            'runNumber'])
            evts = evts[evts.invMass_ECAL_ele > 0]
            # split in categories
            self.data['mee'] = {}
            for cat, m in categories.items():
                sel = m['expr'](evts)
                idx = ak.argsort(evts.eventTime[sel])
                m['data'] = ak.unflatten(evts[sel][idx], counts=min(events_per_bin, len(evts[sel][idx])))
                mass, mass_err = truncated_mean(m['data'])
                self.data['mee'][cat] = {
                    'mass': mass.to_numpy(),
                    'mass_err': mass_err.to_numpy(),
                    'time': ak.mean(m['data'].eventTime, axis=1).to_numpy()}
            # load the luminosity data
            self.load_brilcalc_data(lumi_file=lumi_file, mins=minutes)

    def load_brilcalc_data(self, lumi_file: str, mins: float = 600.):
        """
        Load luminosity from files.

        :param lumi_file: luminosity data from brilcalc.
        :param mins: time bins duration.
        :return: pandas group splitted in bins of `mins` duration.
        """
        df = pd.read_csv(lumi_file, usecols=["run", "fill", "time", "delivered(/ub)"])
        lumi_bins = df.groupby(pd.cut(df['time'],
                                      bins=int(np.ceil((df['time'].max()-df['time'].min())/mins))))
        sel = ~np.isnan(lumi_bins.mean()['time'].values)
        self.data['lumi'] = {
            'time': lumi_bins.mean()['time'].values[sel],
            'inst_lumi': lumi_bins.mean()['delivered(/ub)'].values[sel]/10000/23,
            'int_lumi': lumi_bins.sum()['delivered(/ub)'].values[sel]/1e9}

    def load_histograms(self, json_files: Union[str, List[str]]):
        """
        Load luminosity and mee data from preprocessed file.

        :param json_file: json file names.
        """
        load = lambda x: np.array(x) if isinstance(x, list) else {k: load(v) for k, v in x.items()}
        concat = lambda x, y: {k : np.concatenate([x.get(k, []), y.get(k, [])]) for k in set(x.keys()).union(y.keys())}
        for jf in json_files:
            with open(jf) as jd:
                jdata = load(json.load(jd))
                if 'mee' not in jdata or 'lumi' not in jdata:
                    logging.warning(f'mee or lumi data missing from provided histogram file. Skipping {jf}')
                    continue
                else:
                    logging.info(f'loading: {jf}')
                # duplicate last time bin to smooth out inst lumi plots
                # (essentially setting the inst lumi to zero to avoid the
                #  having the bin in between fills show a constant pedestal
                # add a bin 1 minute after the last one
                jdata['lumi']['time'] = np.append(jdata['lumi']['time'], jdata['lumi']['time'][-1]+60)
                jdata['lumi']['time'] = np.insert(jdata['lumi']['time'], obj=0, values=jdata['lumi']['time'][0]-60)
                # set inst lumi to zero
                jdata['lumi']['inst_lumi'] = np.append(jdata['lumi']['inst_lumi'], 0)
                jdata['lumi']['inst_lumi'] = np.insert(jdata['lumi']['inst_lumi'], obj=0, values=0)
                # duplicate int lumi
                jdata['lumi']['int_lumi'] = np.append(jdata['lumi']['int_lumi'], jdata['lumi']['int_lumi'][-1])
                jdata['lumi']['int_lumi'] = np.insert(jdata['lumi']['int_lumi'], obj=0, values=jdata['lumi']['int_lumi'][0])
                if not self.data:
                    self.data = jdata
                else:
                    # assuming a certain data structure
                    # mee -> categories -> {'mass', 'mass_err', 'time'}
                    for k in set(self.data['mee'].keys()).union(jdata['mee'].keys()):
                        self.data['mee'][k] = concat(self.data['mee'].get(k, {}), jdata['mee'].get(k, {}))
                    # lumi -> {'int_lumi', 'inst_lumi', 'time'}
                    self.data['lumi'] = concat(self.data['lumi'], jdata['lumi'])

    def save_histograms(self, json_file: str):
        """
        Save processed histograms.

        :param json_file: output json file name.
        """
        with open(json_file, "w") as outfile:
            json.dump(self.data, outfile, cls=NumpyArrayEncoder, indent=4)

    def scale_monitoring(self):
        """
        Produce the ECAL energy scale monitoring plots using the Zee invariant mass distribution.

        :return: matplotlib figure.
        """
        fig = plt.figure()
        gs = fig.add_gridspec(2, hspace=0, height_ratios=[2,1])
        axs = gs.subplots(sharex=True)
        
        # mee graphs
        for cat, m in self.data['mee'].items():
            idx = np.argsort(m['time'])
            axs[0].errorbar(x=m['time'][idx].astype(dtype='datetime64[s]'),
                            y=m['mass'][idx], 
                            yerr=m['mass_err'][idx],
                            color=self.colors[cat],
                            label='$\\bf{'+cat+'}$: '+f'mean={np.mean(m["mass"]):.2f}, s.d.={np.std(m["mass"]):.2f}')

        # int lumi histo
        axs_int = axs[1].twinx()
        axs_int.hist(self.data['lumi']['time'].astype(dtype='datetime64[s]'),
                     weights=self.data['lumi']['int_lumi'],
                     bins=len(self.data['lumi']['time']),
                     histtype='stepfilled', cumulative=True,
                     color='yellow', alpha=0.3)
        # inst lumi histo
        idx = np.argsort(self.data['lumi']['time'].astype(dtype='datetime64[s]'))
        axs[1].hist(self.data['lumi']['time'].astype(dtype='datetime64[s]'),
                    weights=self.data['lumi']['inst_lumi'],
                    bins=np.append(self.data['lumi']['time'][idx],
                                   self.data['lumi']['time'][idx][-1]+60).astype(dtype='datetime64[s]'))

        plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%d/%m/%Y'))
        plt.gcf().autofmt_xdate()
        axs[0].set_ylabel('$m_{ee} (GeV)$')
        axs[0].grid()
        axs[0].legend()
        axs[0].set_ylim(85,95)
        axs[1].set_ylabel('$Inst. Lumi.$ \n $(10^{34} cm^{-2} s^{-1})$')
        axs[1].set_xlabel('Date')
        axs[1].set_ylim(0,2.05)
        axs[1].grid()
        axs_int.set_ylabel('$Int. Lumi. (fb^{-1})$')
        plt.axes(axs[0])
        hep.cms.label("Internal", data=True)

        return fig
