Automation workflow docs: Trivial good health check
===================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

runghc.py
=========

.. argparse::
   :filename: ../runghc.py
   :func: get_opts
   :prog: runghc.py

