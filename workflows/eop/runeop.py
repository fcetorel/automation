#!/usr/bin/env python3
import argparse
import subprocess
import sys
import logging
from typing import Dict
from os import remove, path, system, listdir
from itertools import islice
from typing import Optional, List, Tuple
from ecalautoctrl import JobCtrl, HandlerBase, prev_task_data_source, process_by_intlumi
from ecalautoctrl.TaskHandlers import AutoCtrlScriptBase
#from ecalautoctrl.notifications import MattermostHandler
import harness_definition

import ROOT
ROOT.gROOT.SetBatch(1)
ROOT.gErrorIgnoreLevel = ROOT.kWarning #switch off 'Info in <TCanvas::Print>: blabla'


@prev_task_data_source
@process_by_intlumi(target=0.5)

class EopHandler(HandlerBase):
    """
    Execute all the steps to generate the eop monitoring plots.
    Process fills that have been dumped (completed).

    :param task: task name.
    :param prev_input: name of the task from which gather the input data.
    :param deps_tasks: list of workfow dependencies.
    """

    def __init__(self,
                 task: str,
                 prev_input: str,
                 deps_tasks: Optional[List[str]]=None,
                 **kwargs):
        super().__init__(task=task, deps_tasks=deps_tasks, **kwargs)
        self.prev_input = prev_input
        self.submit_parser.add_argument('--cfg',
                                         dest='cfg',
                                         default=None,
                                         type=str,
                                         help='Path to monitoring cfg file')

        self.submit_parser.add_argument('--outdir',
                                         dest='outdir',
                                         default=None,
                                         type=str,
                                         help='Base path of output location')

        self.submit_parser.add_argument('--eosplot',
                                         dest='eosplot',
                                         default=None,
                                         type=str,
                                         help='Base path of plot location on EOS')




        self.resubmit_parser.add_argument('--cfg',
                                         dest='cfg',
                                         default=None,
                                         type=str,
                                         help='Path to monitoring cfg file')

        self.resubmit_parser.add_argument('--outdir',
                                         dest='outdir',
                                         default=None,
                                         type=str,
                                         help='Base path of output location')

        self.resubmit_parser.add_argument('--eosplot',
                                         dest='eosplot',
                                         default=None,
                                         type=str,
                                         help='Base path of plot location on EOS')





    def HarnessLimits(self,harnessname):
    
        if '/' in harnessname:
            for token in harnessname.split('/'):
                if 'IEta' in token:
                    return self.HarnessLimits(token)
    
        return int(harnessname.split('_')[1]), int(harnessname.split('_')[2]), int(harnessname.split('_')[4]), int(harnessname.split('_')[5])
    
    def writeIC(self, icfilename: str=None, ICmap: Dict=None):
        """
        Write a set of IC from a single IOV into a txt file. EB only
        
        Output format: ix iy iz IC IC error
    
        :param icfilename: output file name.
        :param ICmap: dictionary (ieta, iphi) containing the IC values
        """
        with open(icfilename, 'w') as icfile:
            for ieta,iphi_ICmap in ICmap.items():
                for iphi,IC in iphi_ICmap.items():
                    icfile.write("%i\t%i\t0\t%f\t0.\n"%(ieta,iphi,IC))
       
    def icCreator(self, jctrl, runs: List[int], files: List[str]) -> Tuple:
        """
        Process eop data and produce the eop harness corrections.

        :return: ic output, plots.
        """
        runbasedir = f'{self.opts.outdir}/{runs[-1]}/'
        #get the harness ranges
        harness_ranges = harness_definition.GetHarnessRanges()        
        print ("Finally in IC creator") 
       
        #make the monitoring files .cfg
        for ih, harness_range in enumerate(harness_ranges[:3]):
            etamin = harness_range[0]
            etamax = harness_range[1]
            phimin = harness_range[2]
            phimax = harness_range[3]
        
            harnessdir = f"{runbasedir}/IEta_{etamin}_{etamax}_IPhi_{phimin}_{phimax}/"
            system(f"mkdir -p {harnessdir}")
        
            with open(self.opts.cfg) as fi:
                contents = fi.read()
                replaced_contents = contents.replace("INPUTFILES", ' '.join(files))
                replaced_contents = replaced_contents.replace("RUNS", f"{runs[0]} {runs[-1]}")
                replaced_contents = replaced_contents.replace("IETAMIN", str(etamin))
                replaced_contents = replaced_contents.replace("IETAMAX", str(etamax))
                replaced_contents = replaced_contents.replace("IPHIMIN", str(phimin))
                replaced_contents = replaced_contents.replace("IPHIMAX", str(phimax))
                replaced_contents = replaced_contents.replace("OUTPUTFILE", harnessdir+'results.root')
            cfgfilename=harnessdir+"/config.cfg"
            with open(cfgfilename, "w") as fo:
                fo.write(replaced_contents)
        
            system(f'mkdir -p {runbasedir}/logs')
            with open(f'{runbasedir}/logs/eop-'+'_'.join(runs)+'.log', 'w') as logfile:
                ret = subprocess.run(f'$CMSSW_BASE/src/Eop_framework/bin/LaserMonitoring.exe --cfg {cfgfilename} --scaleMonitor',
                                     shell=True,
                                     stdout=logfile,
                                     stderr=logfile)
                if ret.returncode != 0:
                    print ("IM QUITTING in ic creator")
                    self.log.error(f'LaserMonitoring failed. Return code {ret.returncode}')
                    jctrl.failed(jid=0)
                    sys.exit(-1) 
        
        chain_dict = {}
        h2_ic = ROOT.TH2F("eop_map", "E/p map; i#phi; i#eta", 360,0.5,360.5,171,-85.5,85.5)
        for dirname in [path.abspath(f'{runbasedir}/'+d) for d in listdir(f'{runbasedir}')]:
            if dirname.find("IEta")!=-1 and dirname.find("IPhi")!=-1:
                chain_dict[dirname] = ROOT.TChain('prompt_monitoring')
                chain_dict[dirname].Add(dirname+'/*.root')
        
        print("Creating point corrections")
        IOV_list = []
   
        IC = {} #IC is a list of dictionaries, i.e. IC [iIOV] [ix] [iy] 
        # initialize to a value
        

        print("Loop over harnesses")
        for harnessname, chain in chain_dict.items():
            if chain.GetEntries()!=0:
                Npoints = chain.Draw('scale_Eop_median:runmin:lsmin:runmax:lsmax', '', 'goff')
                Escale = chain.GetVal(0)
                runmin = chain.GetVal(1)    
                lsmin = chain.GetVal(2)
                runmax = chain.GetVal(3)
                lsmax = chain.GetVal(4)
                if len(IOV_list)==0:
                    for iIOV in range(0,Npoints):
                        IOV = [int(runmin[iIOV]),int(lsmin[iIOV]),int(runmax[iIOV]),int(lsmax[iIOV])]
                        IOV_list.append(IOV)
                        IC[iIOV] = {}
                        for ieta in range(-85,86):
                            IC[iIOV][ieta]={}
        
                ref_scale = 1.
                for iIOV in range(0,len(IOV_list)):
                    IOV = IOV_list[iIOV]
                    if Npoints!=len(IOV_list):
                        self.log.error(f'Missing IOVs in harness {harnessname}')
                        jctrl.failed(jid=0)                                            
                        sys.exit(-1)
                    if ( runmin[iIOV]!=IOV[0] or lsmin[iIOV]!=IOV[1] or runmax[iIOV]!=IOV[2] or lsmax[iIOV]!=IOV[3] ):
                        self.log.error(f'IOV mismatching in harness {harnessname}')
                        jctrl.failed(jid=0)
                        sys.exit(-1)
                    else:
                        ietamin,ietamax,iphimin,iphimax = self.HarnessLimits(harnessname)
                        for ieta in range(ietamin,ietamax+1):
                            for iphi in range(iphimin,iphimax+1):
                                if( Escale[iIOV]<=0.5 or Escale[iIOV]>2.):
                                    ICvalue = ref_scale                                    
                                else:
                                    ICvalue = ref_scale/Escale[iIOV]
                                ibin= h2_ic.FindBin(iphi,ieta)
                                IC[iIOV][ieta][iphi] = ICvalue
                                h2_ic.SetBinContent(ibin, ICvalue)

        print ('Drawing histos')
        plotdir = f'{runbasedir}/plottoni/'
        system(f'mkdir -p {plotdir}')
        c = ROOT.TCanvas("c_%s"%runs[-1],"c_%s"%runs[-1], 1400, 700)
        c.cd()  
        ROOT.gStyle.SetOptStat(0)
        ROOT.gStyle.SetPalette(55)        
        h2_ic.Draw("colz")
        c.Print(f"{plotdir}/ic_map.png")	
        c.Print(f"{plotdir}/ic_map.root")		
        
        
        print('writing output txt files')
        icdir = f'{runbasedir}/PointCorrections/'
        system(f'mkdir -p {icdir}')
        output = []
        with open(f'{icdir}/IOVdictionary.txt', 'w') as IOVdict_file:
            for iIOV,IOV in enumerate(IOV_list):
                icfilename = f'{icdir}/IC_{IOV[0]}-{IOV[1]}_{IOV[2]}-{IOV[3]}.txt'
                output.append(path.abspath(f'{icfilename}'))
                IOVdict_file.write('%i\t%i\t%i\t%i\t%s\n' % (IOV[0], IOV[1],IOV[2],IOV[3], icfilename))
                self.writeIC(icfilename, IC[iIOV])
 
        return output, f'{icdir}'



    def resubmit(self) -> int:
        """
        Resubmit failed runs.

        :return: status.
        """
        for group in self.groups():  #grouping by int lumi, as specified in the decorator above
            # master run ---> to set the tag in the JobCtrl
            run_i = group[0]
            run_f = group[-1]
            fdict = self.get_files(group) # get the files in the group 
            if fdict is not None and len(fdict)>0:
                jctrl = JobCtrl(task=self.task,
                                campaign=self.campaign,
                                tags={'run_number' : run_f['run_number']},
                                dbname=self.opts.dbname)
                if jctrl.taskExist() and len(jctrl.getFailed())>0:
                    files = jctrl.getJob(jid=0, last=True)[-1]['inputs']
                    try:
                        jctrl.running(jid=0)   
                        self.log.info(f'Resubmitting processing for run: {run_i["run_number"]} -->  {run_f["run_number"]}')
                        ret = self.icCreator(jctrl, runs=[r['run_number'] for r in group], files=[f.split(',')[-1] for f in fdict])
                        if not ret:
                            jctrl.failed(jid=0)
                        else:
                            # mark as completed
                            jctrl.done(jid=0, fields={
                                'output': ','.join(ret[0]),
                                'plots': ret[1]})   #actually no plots are drawn now, to be implemented
                    except Exception as e:
                        print ("CIAOOO exception")
                        jctrl.failed(jid=0)
                        self.log.error(f'Failed producing the IC for run: {run_i["run_number"]} -->  {run_f["run_number"]}: {e}')
                        continue
 

    def submit(self) -> int:
        """
        Submit calibration jobs for runs in eop-new
        """
        print ("Submitting")


        for group in self.groups():  #grouping by int lumi, as specified in the decorator above
             # master run ---> to set the tag in the JobCtrl
             run_i = group[0]
             run_f = group[-1]
             print (f'This run I: {run_i["run_number"]} , run F: {run_f["run_number"]}')
             fdict = self.get_files(group) # get the files in the group 

             if fdict is not None and len(fdict)>0:
                 jctrl = JobCtrl(task=self.task,
                                 campaign=self.campaign,
                                 tags={'run_number' : run_f['run_number']},
                                 dbname=self.opts.dbname)
 

                 if not jctrl.taskExist():
                     jctrl.createTask(jids=[0],
                                     fields=[{
                                         'group': ','.join([r['run_number'] for r in group[:-1]]),
                                         'inputs': ','.join([f.split(',')[-1] for f in fdict])}])

                 try:
                     jctrl.running(jid=0)
                     self.rctrl.updateStatus(run=run_i['run_number'],
                                             status={self.task: 'processing'})
                     for r in group:
                         self.rctrl.updateStatus(run=r['run_number'],
                                                 status={self.task: 'merged'})
                     self.log.info(f'Processing run: {run_i["run_number"]} -->  {run_f["run_number"]}')
                     ret = self.icCreator(jctrl, runs=[r['run_number'] for r in group], files=[f.split(',')[-1] for f in fdict])
                     if not ret:
                         jctrl.failed(jid=0)
                     else:
                         # mark as completed
                         jctrl.done(jid=0, fields={
                             'output': ','.join(ret[0]),
                             'plots': ret[1]})   #actually no plots are drawn now, to be implemented
                         #print(','.join(ret[0]))
                 except Exception as e:
                     jctrl.failed(jid=0)
                     self.log.error(f'Failed producing the IC for run: {run_i["run_number"]} -->  {run_f["run_number"]}: {e}')
                     continue
       


    
if __name__ == '__main__':
    handler = EopHandler(task='eop',
                            deps_tasks=['ecalelf-ntuples-wskim'],
                            prev_input='ecalelf-ntuples-wskim')
    ret = handler()
    sys.exit(ret)

get_opts = AutoCtrlScriptBase.export_options(EopHandler)
