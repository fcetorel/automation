Automation workflow docs: ECAL time calibration
===============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

runreco.py
==========

.. argparse::
   :filename: ../runreco.py
   :func: get_opts
   :prog: runreco.py

