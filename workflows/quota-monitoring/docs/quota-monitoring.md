# PhiSym/EFlow calibration

## Resources
Quick links:

- [Jenkins job](https://dpg-ecal-calib.web.cern.ch/view/ECAL%20Prompt/job/quota-monitoring/) 
- [Automation config](https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/automation/-/tree/master/workflows/quota-monitoring)

## Workflow structure
Run a single script (provided by the `ecalautoctrl` python package) that checks the ECAL DPG usage of its AFS and EOS space.



