#!/bin/bash

CMSSW_REL=$1

# exit when any command fails; be verbose
set -ex

# eos-client ("eos ls" and other stuff, not the actual deamon to mount the filesystem, that's done with the -B option by apptainer)
cat << EOF >>/etc/yum.repos.d/eos7-stable.repo
[eos8-stable]
name=EOS binaries from CERN Linuxsoft [stable]
gpgcheck=0
enabled=1
baseurl=http://linuxsoft.cern.ch/internal/repos/eos8-stable/x86_64/os
priority=9
EOF
yum install -y --nogpgcheck eos-client

# openssl
yum install -y openssl-libs openssl-devel

# enable accessing T0 queues
yum install -y environment-modules
mkdir -p /etc/modulefiles/lxbatch/
cat << EOF >> /etc/modulefiles/lxbatch/tzero
#%Module 1.0
# module file for lxbatch/share
#
setenv _myschedd_POOL tzero
setenv _condor_CONDOR_HOST "norwegianblue02.cern.ch, tzcm1.cern.ch"
EOF

yum install -y bc

# make cmsrel etc. work
shopt -s expand_aliases
source /cvmfs/cms.cern.ch/cmsset_default.sh

git config --global user.name 'ecalgit'
git config --global user.email 'ecalgit@cern.ch'
git config --global user.github 'ecalgit'

mkdir -p /home/ecalgit/
cd /home/ecalgit/
cmsrel $CMSSW_REL 
cd $CMSSW_REL/src
cmsenv
git cms-init
cd -

# manually install the required packages needed for ecalautoctrl 
export PYTHON3PATH=$PYTHON3PATH:/home/ecalgit/lib/python3.9/site-packages/
git clone https://${SERVICE_USERNAME}:${SERVICE_PWD}@gitlab.cern.ch/cmsoms/oms-api-client.git
cd oms-api-client/
python3 -m pip install --prefix /home/ecalgit/ -I .
python3 -m pip install --prefix /home/ecalgit/ -I influxdb
python3 -m pip install --prefix /home/ecalgit/ -I dbs3-client==3.17.0

# install ecalautoctrl
python3 -m pip install git+https://gitlab.cern.ch/cms-ecal-dpg/ECALELFS/automation-control/ --prefix /home/ecalgit/ --no-deps -I
# install phisym
python3 -m pip install git+https://github.com/simonepigazzini/ecalphisym.git --prefix /home/ecalgit/ -I

# copy lib and bin to the CMSSW area
mv /home/ecalgit/bin/* /home/ecalgit/$CMSSW_REL/bin/$SCRAM_ARCH/
mv /home/ecalgit/lib/python3.9/site-packages/* /home/ecalgit/$CMSSW_REL/python/

## ECAL code
cd $CMSSW_BASE/src

# install ECALElf
git clone https://${SERVICE_USERNAME}:${SERVICE_PWD}@gitlab.cern.ch/cms-ecal-dpg/ECALELFS/ECALELF.git

# install conddb interface
git clone https://${SERVICE_USERNAME}:${SERVICE_PWD}@gitlab.cern.ch/cms-ecal-dpg/espresso.git
git clone --no-checkout https://github.com/camendola/usercode.git
cd $CMSSW_BASE/src/usercode
git checkout origin/for_espresso -- DBDump/BuildFile.xml
git checkout origin/for_espresso -- DBDump/interface/CondDBDumper.h
git checkout origin/for_espresso -- DBDump/bin/conddb_dumper.cpp
git checkout origin/for_espresso -- DBDump/interface/HistoManager.h
cp $CMSSW_BASE/src/espresso/etc/conddb_dumper_buildfile.xml DBDump/bin/BuildFile.xml
cd $CMSSW_BASE/src

# PulseShapes
git clone -b 13X https://github.com/simonepigazzini/EcalReconstruction.git

# Timing
git clone -b automation https://github.com/simonepigazzini/EcalTiming.git

# Alignment
mkdir EcalValidation
git clone -b CMSSW_12_3_0_pre5 https://github.com/simonepigazzini/EcalAlignment.git EcalValidation/EcalAlignment

# pi0
git clone -b 12_2_X_pi0_monitoring https://github.com/ECALELFS/ECALpro.git CalibCode

# compile
cmsenv
scram b -j

# cmake (needed by EoP)
dnf install -y cmake

# EoP
git clone https://github.com/fabio-mon/Eop_framework.git
mkdir Eop_framework/build 
cd Eop_framework/build
cmake ..
make
cd -

# brilcalc
export PATH=$HOME/.local/bin:/cvmfs/cms-bril.cern.ch/brilconda/bin:$PATH
pip install --user brilws

# set w/r permissions on home directory
chmod 777 /home/ecalgit/

# setup script
cat << EOF >> /home/ecalgit/setup.sh
#!/bin/bash

source /usr/share/Modules/init/sh
source /cvmfs/cms.cern.ch/cmsset_default.sh
cd /home/ecalgit/$CMSSW_REL
cmsenv
export PATH=\$HOME/.local/bin:/cvmfs/cms-bril.cern.ch/brilconda/bin:\$PATH
cd -
EOF
